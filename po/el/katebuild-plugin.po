# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2008.
# Dimitrios Glentadakis <dglent@gmail.com>, 2012.
# Stelios <sstavra@gmail.com>, 2012, 2015, 2018, 2019, 2020, 2021, 2022.
# Dimitris Kardarakos <dimkard@gmail.com>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-13 00:40+0000\n"
"PO-Revision-Date: 2022-12-30 15:31+0200\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stelios"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sstavra@gmail.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Έξοδος"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "Κατασκευή ξανά"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "Ακύρωση"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr ""

#: buildconfig.cpp:27
#, kde-format
msgid "Automatically switch to output pane on executing the selected target"
msgstr ""

#: buildconfig.cpp:40
#, fuzzy, kde-format
#| msgid "Build again"
msgid "Build & Run"
msgstr "Κατασκευή ξανά"

#: buildconfig.cpp:46
#, fuzzy, kde-format
#| msgid "Build and Run Selected Target"
msgid "Build & Run Settings"
msgstr "Κατασκευή και εκτέλεση επιλεγμένου προορισμού"

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1232
#, kde-format
msgid "Build"
msgstr "Κατασκευή"

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr "Επιλογή προορισμού..."

#: plugin_katebuild.cpp:227
#, kde-format
msgid "Build Selected Target"
msgstr "Κατασκευή επιλεγμένου προορισμού"

#: plugin_katebuild.cpp:232
#, kde-format
msgid "Build and Run Selected Target"
msgstr "Κατασκευή και εκτέλεση επιλεγμένου προορισμού"

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "Διακοπή"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr ""

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr ""

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Ρυθμίσεις προορισμού"

#: plugin_katebuild.cpp:403
#, fuzzy, kde-format
#| msgid "Build Command:"
msgid "Build Information"
msgstr "Εντολή κατασκευής:"

#: plugin_katebuild.cpp:620
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "Δεν υπάρχει αρχείο ή κατάλογος καθορισμένο για κατασκευή."

#: plugin_katebuild.cpp:624
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"Το αρχείο \"%1\" δεν είναι ένα τοπικό αρχείο. Μη τοπικά αρχεία δεν είναι "
"δυνατό να μεταγλωττιστούν."

#: plugin_katebuild.cpp:686
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"Αδυναμία εκτέλεσης εντολής: %1\n"
"Η διαδρομή δεν υπάρχει: %2"

#: plugin_katebuild.cpp:700
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Αποτυχία εκτέλεσης \"%1\". κατάστασηΕξόδου = %2"

#: plugin_katebuild.cpp:715
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "Η κατασκευή του <b>%1</b> ακυρώθηκε"

#: plugin_katebuild.cpp:822
#, kde-format
msgid "No target available for building."
msgstr "Μη διαθέσιμος προορισμός προς κατασκευή."

#: plugin_katebuild.cpp:836
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "Δεν υπάρχει τοπικό αρχείο ή κατάλογος καθορισμένος για κατασκευή."

#: plugin_katebuild.cpp:842
#, kde-format
msgid "Already building..."
msgstr "Υπό κατασκευή..."

#: plugin_katebuild.cpp:864
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Κατασκευή προορισμού <b>%1</b> ..."

#: plugin_katebuild.cpp:878
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Αποτελέσματα Make:</title><nl/>%1"

#: plugin_katebuild.cpp:914
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:920
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Βρέθηκε ένα σφάλμα."
msgstr[1] "Βρέθηκαν %1 σφάλματα."

#: plugin_katebuild.cpp:924
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Βρέθηκε μία προειδοποίηση."
msgstr[1] "Βρέθηκαν %1 προειδοποιήσεις."

#: plugin_katebuild.cpp:927
#, fuzzy, kde-format
#| msgid "Found one error."
#| msgid_plural "Found %1 errors."
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Βρέθηκε ένα σφάλμα."
msgstr[1] "Βρέθηκαν %1 σφάλματα."

#: plugin_katebuild.cpp:932
#, kde-format
msgid "Build failed."
msgstr "Η κατασκευή απέτυχε."

#: plugin_katebuild.cpp:934
#, kde-format
msgid "Build completed without problems."
msgstr "Ολοκλήρωση κατασκευής χωρίς προβλήματα."

#: plugin_katebuild.cpp:939
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:963
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "Αδυναμία εκτέλεσης: %1 Δεν έχει ρυθμιστεί ο κατάλογος εργασίας."

#: plugin_katebuild.cpp:1189
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark an error."
#| msgid "error"
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "σφάλμα"

#: plugin_katebuild.cpp:1192
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark a warning."
#| msgid "warning"
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "προειδοποίηση"

#: plugin_katebuild.cpp:1195
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1198
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "μη ορισμένη αναφορά"

#: plugin_katebuild.cpp:1231 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr "Σύνολο προορισμών"

#: plugin_katebuild.cpp:1233
#, kde-format
msgid "Clean"
msgstr "Καθαρισμός"

#: plugin_katebuild.cpp:1234
#, kde-format
msgid "Config"
msgstr "Διαμόρφωση"

#: plugin_katebuild.cpp:1235
#, kde-format
msgid "ConfigClean"
msgstr "ΚαθαρισμόςΔιαμόρφωσης"

#: plugin_katebuild.cpp:1426
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr ""

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>Π:</B> %1"

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>Καταλ:</B> %1"

#: TargetHtmlDelegate.cpp:101
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"Να μείνει κενό για να χρησιμοποιήσει τον κατάλογο του τρέχοντος εγγράφου.\n"
"Να προστεθούν κατάλογοι αναζήτησης προσθέτοντας διαδρομές με διαχωριστικό το "
"';'"

#: TargetHtmlDelegate.cpp:108
#, kde-format
msgid ""
"Use:\n"
"\"%B\" for project base directory\n"
"\"%b\" for name of project base directory"
msgstr ""

#: TargetHtmlDelegate.cpp:111
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Χρήση:\n"
"\"%f\" για το τρέχον αρχείο\n"
"\"%d\" για τον κατάλογο του τρέχοντος αρχείου\"%n\" για το τρέχον όνομα "
"αρχείου χωρίς κατάληξη"

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr ""

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr ""

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr "Εντολή/Όνομα συνόλου προορισμών"

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr "Κατάλογος εργασίας / Εντολή"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr "Εκτέλεση εντολής"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""
"Φιλτράρισμα προορισμών, με τα βελάκια επιλέγονται, με το Enter εκτελούνται"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Δημιουργία νέου συνόλου προορισμών"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "Αντιγραφή εντολής ή συνόλου προορισμών"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "Διαγραφή του τρέχοντος προορισμού ή του τρέχοντος συνόλου προορισμών"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Προσθήκη νέου προορισμού"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Κατασκευή επιλεγμένου προορισμού"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "Κατασκευή και εκτέλεση επιλεγμένου προορισμού"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "Μετακίνηση επιλεγμένου προορισμού επάνω"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "Μετακίνηση επιλεγμένου προορισμού κάτω"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Κατασκευή"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "Εισαγωγή διαδρομής"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "Επιλογή καταλόγου για εισαγωγή"

#~ msgid "Project Plugin Targets"
#~ msgstr "Προορισμοί πρόσθετου του έργου"

#~ msgid "build"
#~ msgstr "κατασκευή"

#~ msgid "clean"
#~ msgstr "καθαρισμός"

#~ msgid "quick"
#~ msgstr "γρήγορα"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "Η κατασκευή του <b>%1</b> ολοκληρώθηκε."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "Η κατασκευή του <b>%1</b> παρουσίασε σφάλματα."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "Η κατασκευή του <b>%1</b> παρουσίασε προειδοποιήσεις."

#~ msgid "Show:"
#~ msgstr "Εμφάνιση:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "Αρχείο"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Γραμμή"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Μήνυμα"

#~ msgid "Next Error"
#~ msgstr "Επόμενο σφάλμα"

#~ msgid "Previous Error"
#~ msgstr "Προηγούμενο σφάλμα"

#~ msgid "Show Marks"
#~ msgstr "Εμφάνιση σημείων"

#~ msgctxt "@info"
#~ msgid ""
#~ "<title>Could not open file:</title><nl/>%1<br/>Try adding a search path "
#~ "to the working directory in the Target Settings"
#~ msgstr ""
#~ "<title>Αδυναμία ανοίγματος του αρχείου:</title><nl/>%1<br/>Δοκιμάστε να "
#~ "προσθέσετε μια διαδρομή αναζήτησης στον κατάλογο εργασίας στις ρυθμίσεις "
#~ "προορισμού"

#~ msgid "Error"
#~ msgstr "Σφάλμα"

#~ msgid "Warning"
#~ msgstr "Προειδοποίηση"

#~ msgid "Only Errors"
#~ msgstr "Μόνο σφάλματα"

#~ msgid "Errors and Warnings"
#~ msgstr "Σφάλματα και προειδοποιήσεις"

#~ msgid "Parsed Output"
#~ msgstr "Αναλυμένη έξοδος"

#~ msgid "Full Output"
#~ msgstr "Πλήρης έξοδος"

#~ msgid ""
#~ "Check the check-box to make the command the default for the target-set."
#~ msgstr ""
#~ "Επιλέξτε το πλαίσιο ελέγχου για να κάνετε την εντολή προκαθορισμένη για "
#~ "το σύνολο προορισμών."

#~ msgid "Select active target set"
#~ msgstr "Επιλογή του ενεργού συνόλου προορισμών"

#, fuzzy
#~| msgid "Build selected target"
#~ msgid "Filter targets"
#~ msgstr "Κατασκευή επιλεγμένου προορισμού"

#~ msgid "Build Default Target"
#~ msgstr "Κατασκευή προκαθορισμένου προορισμού"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "Κατασκευή προκαθορισμένου προορισμού"

#~ msgid "Build Previous Target"
#~ msgstr "Κατασκευή του προηγούμενου προορισμού"

#~ msgid "Active target-set:"
#~ msgstr "Ενεργό σύνολο προορισμών:"

#~ msgid "config"
#~ msgstr "διαμόρφωση"

#~ msgid "Kate Build Plugin"
#~ msgstr "Πρόσθετο κατασκευής Kate"

#~ msgid "Select build target"
#~ msgstr "Επιλογή προορισμού κατασκευής"

#~ msgid "Filter"
#~ msgstr "Φίλτρο"

#~ msgid "Build Output"
#~ msgstr "Έξοδος κατασκευής"

#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>Αδυναμία ανοίγματος αρχείου:</title><nl/>%1"

#~ msgid "Next Set of Targets"
#~ msgstr "Επόμενο σύνολο προορισμών"

#~ msgid "No previous target to build."
#~ msgstr "Κανένας προηγούμενος προορισμός για κατασκευή."

#~ msgid "No target set as default target."
#~ msgstr "Δεν έχει ρυθμιστεί κανένας προορισμός ως προκαθορισμένος."

#~ msgid "No target set as clean target."
#~ msgstr "Δεν έχει ρυθμιστεί κανένας προορισμός ως καθαρός προορισμός."

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "Ο προορισμός \"%1\" δε βρέθηκε ώστε να κατασκευαστεί."

#~ msgid "Really delete target %1?"
#~ msgstr "Επιθυμείτε πραγματικά τη διαγραφή του %1;"

#~ msgid "Nothing built yet."
#~ msgstr "Τίποτα δεν κατασκευάστηκε ακόμα."

#~ msgid "Target Set %1"
#~ msgstr "Σύνολο προορισμών %1"

#~ msgid "Target"
#~ msgstr "Προορισμός"

#~ msgid "Target:"
#~ msgstr "Προορισμός:"

#~ msgid "from"
#~ msgstr "από"

#~ msgid "Sets of Targets"
#~ msgstr "Σύνολα προορισμών"

#~ msgid "Make Results"
#~ msgstr "Αποτελέσματα make"

#~ msgid "Others"
#~ msgstr "Άλλα"

#~ msgid "Quick Compile"
#~ msgstr "Γρήγορη μεταγλώττιση"

#~ msgid "The custom command is empty."
#~ msgstr "Η προσαρμοσμένη εντολή είναι κενή."

#~ msgid "New"
#~ msgstr "Νέο"

#~ msgid "Copy"
#~ msgstr "Αντιγραφή"

#~ msgid "Delete"
#~ msgstr "Διαγραφή"

#~ msgid "Quick compile"
#~ msgstr "Γρήγορη μεταγλώττιση"

#~ msgid "Run make"
#~ msgstr "Εκτέλεση make"

#~ msgid "Break"
#~ msgstr "Διακοπή"

#, fuzzy
#~| msgid "There is no file or directory specified for building."
#~ msgid "There is no file to compile."
#~ msgstr "Δεν υπάρχει αρχείο ή κατάλογος καθορισμένο για κατασκευή."
